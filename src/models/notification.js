export class Notification {

  constructor(attributes = {}) {
    this.id = attributes.id;
    this.skillName = attributes.skillName;
    this.ownerId = attributes.ownerId;
  }

}

